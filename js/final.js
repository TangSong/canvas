//config
var config = {
    title: "Shooter Game",
    author: "TangSong",
    version: "1.0",
    control: {
        up: "87", //w
        right: "68", //d
        down: "83", //s
        left: "65" //a
    }
}

//object
var sprite = {
    ship: [{
        name: "ship1_blue",
        width: 99,
        height: 75,
        x: 0,
        y: 0,
        life: 10,
        speed: 400,
        angle: 0,
        img: "../asset/SpaceShooterRedux/PNG/playerShip1_blue.png"
    }, {
        name: "ship2_green",
        width: 50,
        height: 38,
        x: 0,
        y: 0,
        speed: 0,
        angle: 0,
        img: "../asset/SpaceShooterRedux/PNG/playerShip2_green.png"
    }, {
        name: "ship3_red",
        width: 50,
        height: 38,
        x: 0,
        y: 0,
        speed: 0,
        angle: 0,
        img: "../asset/SpaceShooterRedux/PNG/playerShip3_red.png"
    }, {
        name: "ufo_yellow",
        width: 50,
        height: 50,
        x: 0,
        y: 0,
        speed: 0,
        angle: 0,
        img: "../asset/SpaceShooterRedux/PNG/ufoYellow.png"
    }],
    bullet: {
        x: 0,
        y: -60,
        width: 9,
        height: 54,
        speed: 650,
        angle: 0,
        visiable: false,
        img: "../asset/SpaceShooterRedux/PNG/Lasers/laserBlue01.png"
    },
    rock: [{
        width: 101,
        height: 84,
        x: 0,
        y: -84,
        speed: 200,
        angle: 0,
        rotateSpeed: 0,
        img: "../asset/SpaceShooterRedux/PNG/Meteors/meteorBrown_big1.png"
    }, {}],
    button: {
        img1: "../asset/UIpack_RPG/PNG/buttonLong_grey.png",
        img2: "../asset/UIpack_RPG/PNG/buttonLong_grey_pressed.png",
        btn1: {
            name: "start",
            x: 305,
            y: 400,
            width: 190,
            height: 49,
            isDown: false
        }
    },
    background: {
        menu: "../asset/SpaceShooterRedux/Backgrounds/darkPurple.png"
    },
    mouseIcon: {
        onBtn: "../asset/UIpack_RPG/PNG/cursorHand_grey.png",
        up: "../asset/UI_pack_Space/PNG/cursor_pointer3D_shadow.png",
        down: "../asset/UI_pack_Space/PNG/cursor_pointer3D.png"
    },
    lifeIcon: {
        width: 33,
        height: 26,
        img: "../asset/SpaceShooterRedux/PNG/UI/playerLife1_blue.png"
    }
};

//DOM
var myCanvas = document.getElementById('myCanvas');
var ctx = myCanvas.getContext('2d');
//image
var ship1, ship2, ship3, ship4; //飛船
var rock; //隕石
var mouseU, mouseD, mouseOnBtn;
var btn1;
var bullet1;
var lifeIcon;
var isGameStart = false;
var isGameOver = false;
var hard = 1.0;
var score = 0;

ship1 = new Image();
ship2 = new Image();
ship3 = new Image();
ship4 = new Image();
rock = new Image();
mouseU = new Image();
mouseD = new Image();
mouseOnBtn = new Image();
btn1 = new Image();
bullet1 = new Image();
lifeIcon = new Image();
ship1.src = sprite.ship[0].img;
ship2.src = sprite.ship[1].img;
ship3.src = sprite.ship[2].img;
ship4.src = sprite.ship[3].img;
rock.src = sprite.rock[0].img;
mouseU.src = sprite.mouseIcon.up;
mouseD.src = sprite.mouseIcon.down;
mouseOnBtn.src = sprite.mouseIcon.onBtn;
btn1.src = sprite.button.img1;
bullet1.src = sprite.bullet.img;
lifeIcon.src = sprite.lifeIcon.img;

//input state
var keysDown = {};
var mouse = {
    x: 0,
    y: 0,
    isDown: false
};


//Listener
window.addEventListener('keydown', function (event) {
    keysDown[event.keyCode] = true;
});
window.addEventListener('keyup', function (event) {
    delete keysDown[event.keyCode];
});
document.addEventListener("mousemove", function (event) {
    mouse.x = event.layerX;
    mouse.y = event.layerY;
}, false);
document.addEventListener("mousedown", function (event) {
    mouse.isDown = true;

    if (sprite.bullet.visiable == false) {
        sprite.bullet.x = sprite.ship[0].x + sprite.ship[0].width / 2 - sprite.bullet.width / 2;
        sprite.bullet.y = sprite.ship[0].y;
        sprite.bullet.visiable = true;
    }
}, false);
document.addEventListener("mouseup", function (event) {
    mouse.isDown = false;
    if (mouseCollides(sprite.button.btn1)) {

        score = 0;
        isGameStart = true;
    }
}, false);

function setup() {
    sprite.ship[0].x = myCanvas.width / 2 - sprite.ship[0].width / 2;

    sprite.ship[0].y = myCanvas.height - sprite.ship[0].height;
}

function draw() {

    ctx.clearRect(0, 0, 800, 600);
    Layer0();
    UI();
    drawMouse();
}

function Layer0() {
    ctx.save();
    ctx.drawImage(rock, sprite.rock[0].x, sprite.rock[0].y);
    ctx.drawImage(bullet1, sprite.bullet.x, sprite.bullet.y);
    if (collides(sprite.bullet, sprite.rock[0])) {
        sprite.rock[0].x = Math.floor((Math.random() * 750) + 1);
        sprite.rock[0].y = -84;

        sprite.bullet.y = -60;
        sprite.bullet.visiable = false;
        score += 100;
    }
    if (collides(sprite.ship[0], sprite.rock[0])) {
        sprite.rock[0].x = Math.floor((Math.random() * 750) + 1);
        sprite.rock[0].y = -84;

        sprite.bullet.y = -60;
        sprite.bullet.visiable = false;
        sprite.ship[0].x = myCanvas.width / 2 - sprite.ship[0].width / 2;
        sprite.ship[0].y = myCanvas.height - sprite.ship[0].height;
        sprite.ship[0].life -= 1;
    }

    ctx.drawImage(ship1, sprite.ship[0].x, sprite.ship[0].y);
    ctx.restore();
}

function UI() {
    var x = 0;
    var textY = 4;
    if (sprite.ship[0].life > 0) {
        if (isGameStart == false) {
            //button
            if (mouseCollides(sprite.button.btn1)) {

                if (mouse.isDown == false) {
                    btn1.src = sprite.button.img1;
                } else {
                    btn1.src = sprite.button.img2;
                    x = 4;
                }
            } else {
                btn1.src = sprite.button.img1;
                x = 0;
            }
            ctx.drawImage(btn1, sprite.button.btn1.x, sprite.button.btn1.y + x);

            if (isGameOver) {

                ctx.font = "50px Arial";
                ctx.fillStyle = "red";
                ctx.fillText("Game Over", 280, 220);
                ctx.font = "20px Arial";

                ctx.fillText("High Score: " + Math.floor(score), 330, 320);
            } else {
                ctx.font = "50px Arial";
                ctx.fillStyle = "red";
                ctx.fillText(config.title, 240, 220);
                ctx.font = "20px Arial";
            }


            ctx.fillStyle = "#4e6367";
            if (mouseCollides(sprite.button.btn1)) {
                if (mouse.isDown == false) {
                    textY = 4;
                } else {

                    ctx.fillStyle = "#30deed";
                    textY = 8;
                }
            }
            ctx.fillText(sprite.button.btn1.name,
                sprite.button.btn1.x + sprite.button.btn1.width / 2 - 17,
                sprite.button.btn1.y + sprite.button.btn1.height / 2 + textY);

        } else {
            ctx.font = "30px Arial";
            ctx.fillStyle = "red";
            ctx.fillText("score: " + Math.floor(score), 10, 80);

        }

        for (var i = 0; i < sprite.ship[0].life; i++) {
            ctx.drawImage(lifeIcon, 40 * i + 10, 10);
        }
    } else {
        sprite.rock[0].speed = 200;
        sprite.ship[0].speed = 400;
        sprite.bullet.speed = 650;
        sprite.ship[0].life = 10;
        isGameStart = false;
        isGameOver = true;
    }

}

function drawMouse() {
    if (mouseCollides(sprite.button.btn1)) {
        mouseU.src = sprite.mouseIcon.onBtn;
        mouseD.src = sprite.mouseIcon.onBtn;
    } else {
        mouseU.src = sprite.mouseIcon.up;
        mouseD.src = sprite.mouseIcon.down;
    }
    if (mouse.isDown == false) {
        ctx.drawImage(mouseU, mouse.x, mouse.y);
    } else {
        ctx.drawImage(mouseD, mouse.x, mouse.y + 3);
    }
}

function mouseCollides(a) {
    return a.x < mouse.x &&
        a.x + a.width > mouse.x &&
        a.y < mouse.y &&
        a.y + a.height > mouse.y;
}

function collides(a, b) {
    return a.x < b.x + b.width &&
        a.x + a.width > b.x &&
        a.y < b.y + b.height &&
        a.y + a.height > b.y;
}
var test = 0;

function objMotion(mod) {
    if (isGameStart) {
        sprite.rock[0].y += sprite.rock[0].speed * mod;
        if (sprite.rock[0].y > 690) {
            sprite.rock[0].x = Math.floor((Math.random() * 750) + 1);
            sprite.rock[0].y = -84;
            sprite.ship[0].life -= 1;
        }
        if (sprite.bullet.y < -59) {
            sprite.bullet.y = -60;
            sprite.bullet.visiable = false;
        } else {
            sprite.bullet.y -= sprite.bullet.speed * mod;
        }
        if (config.control.left in keysDown) { //left
            sprite.ship[0].x -= sprite.ship[0].speed * mod;
        }
        if (config.control.up in keysDown) { //up
            sprite.ship[0].y -= sprite.ship[0].speed * mod;
        }
        if (config.control.right in keysDown) { //right
            sprite.ship[0].x += sprite.ship[0].speed * mod;
        }
        if (config.control.down in keysDown) { //down
            sprite.ship[0].y += sprite.ship[0].speed * mod;
        }
        if (sprite.ship[0].x < 0) {
            sprite.ship[0].x = 1;
        }
        if (sprite.ship[0].x > 800 - sprite.ship[0].width) {
            sprite.ship[0].x = 800 - sprite.ship[0].width - 1;
        }
        if (sprite.ship[0].y > myCanvas.height - sprite.ship[0].height + 1) {

            sprite.ship[0].y = myCanvas.height - sprite.ship[0].height;
        }
        sprite.rock[0].speed += mod * 10;
        sprite.ship[0].speed += mod * 10;
        sprite.bullet.speed += mod * 10;
        score += mod * 10;
    }
}

function run() {
    objMotion((Date.now() - time) / 1000);
    draw();
    time = Date.now();
    setTimeout(run, 1000 / 60);
}

setup();
var time = Date.now();
run();





//
