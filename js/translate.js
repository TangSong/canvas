//object
var sprite = {
    ship1: {
        name: "",
        width: 99,
        height: 75,
        x: 0,
        y: 0,
        speed: 0,
        angle: 0,
        img: "../asset/SpaceShooterRedux/PNG/playerShip1_blue.png"
    },
    rock: {
        width: 0,
        height: 0,
        x: 0,
        y: 0,
        speed: 0,
        angle: 0,
        img: "../asset/SpaceShooterRedux/PNG/Meteors/meteorBrown_big1.png"
    }
};

//code

var myCanvas = document.getElementById('translate');
myCanvas.style.border = "1";
myCanvas.style.borderStyle = "solid";
myCanvas.style.borderColor = "black";
myCanvas.style.width = "800px";
myCanvas.style.height = "600px";

var ctx = myCanvas.getContext('2d');

var ship1 = new Image();
var rock = new Image();

ship1.src = sprite.ship1.img;
rock.src = sprite.ship1.img;

ctx.save();
ship1.onload = function (event) {
    ctx.drawImage(ship1, sprite.ship1.x, sprite.ship1.y);
}

ctx.translate(100, 100);
